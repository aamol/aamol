.. image:: https://gitlab.com/aamol/aamol/badges/master/coverage.svg
.. image:: https://badge.fury.io/py/aamol.svg
    :target: https://pypi.org/project/aamol/

=====
AAMol
=====

...

.. admonition:: Features

    * ...
    * ...

Quick links:

* Code: https://gitlab.com/aamol/aamol/
* Issues: https://gitlab.com/aamol/aamol/issues/


Examples
--------

...


Installation
============

... has only one dependency: Python_ version 3.6 or later.

Install AAMol from PyPI_ with *pip*::

    $ python3 -m pip install aamol

Enable bash tab-completion for future terminal sessions like this::

    $ aamol --completion >> ~/.profile

Run the tests::

    $ pytest ...

and report any errors you get on our `issue tracker`_.


.. _Python: https://python.org/
.. _PyPI: https://pypi.org/project/aamol/
