import re
from pathlib import Path
from setuptools import setup, find_packages

txt = Path('aamol/__init__.py').read_text()
version = re.search("__version__ = '(.*)'", txt).group(1)

long_description = Path('README.rst').read_text()

setup(name='aamol',
      version=version,
      description='Ascii-art molecules',
      long_description=long_description,
      author='J. J. Mortensen',
      author_email='jj@smoerhul.dk',
      packages=find_packages(),
      install_requires=['ase'],
      entry_points={'console_scripts': ['aamol = aamol.cli:main']},
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Environment :: Console',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: '
          'GNU General Public License v3 or later (GPLv3+)',
          'Operating System :: Unix',
          'Programming Language :: Python :: 3 :: Only',
          'Programming Language :: Python :: 3.6',
          'Programming Language :: Python :: 3.7',
          'Programming Language :: Python :: 3.8',
          'Topic :: Scientific/Engineering'])
