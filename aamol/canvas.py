"""Canvas for drawing on."""
from math import inf
from typing import List, Sequence, Dict, Tuple

import numpy as np

A = 0.5  # aspect ratio


class Canvas:
    """A width times height matrix of characters and colors."""
    def __init__(self, size: Tuple[int, int]):
        self.size = size
        self.real_size = np.array([size[0], size[1] / A])

        self.chars = np.empty(size, np.int8)
        self.fg = np.empty(size, np.int8)  # foreground color
        self.bg = np.empty(size, np.int8)  # background color
        self.zfg = np.empty(size)  # foreground depth
        self.zbg = np.empty(size)  # background depth

        self.colors: Dict[str, int] = {}  # maps name to index in self.rgb
        self.rgb: List[Tuple[float, float, float]] = []

        self.add_color('bg', (0.93, 0.93, 0.93))
        self.add_color('black', (0, 0, 0))
        self.add_color('cell', (0.27, 0.15, 0.15))
        self.add_color('orange', (0.93, 0.93, 0.8))

        self._lines: List[str] = []

    def add_color(self, name: str, rgb: Tuple[float, float, float]) -> None:
        """Add new color."""
        self.colors[name] = len(self.colors)
        self.rgb.append(rgb)

    def initialize_colorbar_colors(self) -> None:
        """Initialize 21 colorbar colors from CM-0 to CM-20."""
        if 'CM-0' not in self.colors:
            # Viridis from matplotlib:
            for i, rgb in enumerate([(0.27, 0.00, 0.33),
                                     (0.28, 0.07, 0.40),
                                     (0.28, 0.14, 0.46),
                                     (0.27, 0.20, 0.50),
                                     (0.25, 0.27, 0.53),
                                     (0.23, 0.32, 0.55),
                                     (0.21, 0.37, 0.55),
                                     (0.18, 0.42, 0.56),
                                     (0.16, 0.47, 0.56),
                                     (0.14, 0.52, 0.56),
                                     (0.13, 0.57, 0.55),
                                     (0.12, 0.61, 0.54),
                                     (0.13, 0.66, 0.52),
                                     (0.19, 0.70, 0.49),
                                     (0.27, 0.75, 0.44),
                                     (0.37, 0.79, 0.38),
                                     (0.48, 0.82, 0.32),
                                     (0.61, 0.85, 0.24),
                                     (0.74, 0.87, 0.15),
                                     (0.88, 0.89, 0.10),
                                     (0.99, 0.91, 0.14)]):
                color = f'CM-{i}'
                self.add_color(color, rgb)

    def clear(self) -> None:
        """Clear canvas."""
        self.chars[:] = 32
        self.fg[:] = 0
        self.bg[:] = 0
        self.zfg[:] = -inf
        self.zbg[:] = -inf
        self._lines = []

    @property
    def lines(self) -> List[str]:
        """Convert to list of strings."""
        if not self._lines:
            self._lines = [line.tobytes().decode()
                           for line in self.chars.T]
        return self._lines

    def __str__(self) -> str:
        return '\n'.join(self.lines)

    def write(self,
              position: Sequence[float],
              char: str,
              color: str = 'black') -> None:
        """Write text at position."""
        x, y, z = position
        i = int(x)

        if len(char) != 1:
            for i, c in enumerate(char, i):
                self.write((i, y, z), c, color)
            return

        j = int(y * A)
        I, J = self.size
        if 0 <= i < I and 0 <= j < J:
            if z >= self.zfg[i, j]:
                self.fg[i, j] = self.colors[color]
                self.zfg[i, j] = z
                self.chars[i, j] = ord(char)

    def paste(self,
              text: str,
              fg: str = 'black',
              bg: str = 'orange',
              x: int = None,
              y: int = None,
              pad: int = 0) -> None:
        """Paste block of text."""
        I, J = self.size
        textlines = text.splitlines()
        if pad:
            textlines = [''] + [f' {line} ' for line in textlines] + ['']
        i = max(len(line) for line in textlines)
        j = len(textlines)
        if x is None:
            x = (I - i) // 2
        elif x < 0:
            x = I + x - i + 1
        if y is None:
            y = (J - j) // 2
        elif y < 0:
            y = J + y - j + 1
        if x < 0 or x + i > I or y < 0 or y + j > J:
            return
        self.fg[x:x + i, y:y + j] = self.colors[fg]
        self.bg[x:x + i, y:y + j] = self.colors[bg]
        lines = self.lines
        for n, line in enumerate(textlines):
            old = lines[y + n]
            lines[y + n] = old[:x] + line + ' ' * (i - len(line)) + old[x + i:]

    def circle(self,
               position: Sequence[float],
               radius: float, color: str) -> None:
        """Draw circle at position."""
        x, y, z = position
        I, J = self.size

        i1 = max(0, int(round(x - radius)))
        i2 = min(I, int(round(x + radius)))
        j1 = max(0, int(round(y * A - radius)))
        j2 = min(J, int(round(y * A + radius)))

        if i1 >= i2 or j1 >= j2:
            return

        cid = self.colors[color]

        ij = np.indices([i2 - i1, j2 - j1]).reshape((2, -1)).T + [i1, j1]
        zz = radius**2 - ((ij * [1, 1 / A] - [x, y])**2).sum(1)
        inside = zz > 0.0
        ij = ij[inside]
        Z = zz[inside]**0.5 + z
        ok = Z >= self.zbg[tuple(ij.T)]
        ij1 = tuple(ij[ok].T)
        self.zbg[ij1] = Z[ok]
        self.bg[ij1] = cid
        ok = Z >= self.zfg[tuple(ij.T)]
        ij1 = tuple(ij[ok].T)
        self.zfg[ij1] = Z[ok]
        self.chars[ij1] = 32

    def line(self,
             p1: np.ndarray,
             p2: np.ndarray,
             color: str) -> None:
        """Draw line."""
        i1 = round(p1[0])
        j1 = round(p1[1] * A)
        i2 = round(p2[0])
        j2 = round(p2[1] * A)
        p12 = p2 - p1
        with np.errstate(divide='ignore', invalid='ignore'):
            t = p12[1] / p12[0]
        if t > 2:
            char = '|'
        elif t > 0.5:
            char = '\\'
        elif t > -0.5:
            char = '-'
        elif t > -2:
            char = '/'
        else:
            char = '|'
        if abs(i1 - i2) < abs(j1 - j2):
            y = (np.arange(min(j1, j2), max(j1, j2)) + 0.5) / A
            c = (y - p1[1]) / p12[1]
            points = p1 + c[:, np.newaxis] * p12
            for p in points:
                self.write(p, char, color)
        else:
            x = np.arange(min(i1, i2), max(i1, i2)) + 0.5
            c = (x - p1[0]) / p12[0]
            points = p1 + c[:, np.newaxis] * p12
            for p in points:
                self.write(p, char, color)
