"""Python API."""
from typing import List

from .molecule import Molecule
from .ui import UserInterface


def open_molecules(molecules: List[Molecule],
                   keys: List[str] = None,
                   ui: UserInterface = None,
                   show_help_message: bool = False,
                   verbose: bool = False) -> None:
    """Open window and show molecules."""
    from .aamol import AAMol
    if ui is None:
        from .curses_ui import CursesUserInterface
        ui = CursesUserInterface(keys)
    with ui:
        aam = AAMol(molecules, ui, show_help_message=show_help_message)
        aam.run()
    if verbose:
        print(aam.output.getvalue(), end='')
