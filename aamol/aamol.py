"""The heart of AAMol."""
import sys
from io import StringIO
from time import time
from typing import List, Dict, Callable, TextIO

import numpy as np

from .canvas import Canvas
from .molecule import Molecule
from .plot_atoms import plot
from .ui import UserInterface
from .l10n import _
from .view import View

KeyMeth = Callable[['AAMol'], None]  # type hint

callbacks: Dict[str, KeyMeth] = {}


def key(name: str) -> Callable[[KeyMeth], KeyMeth]:
    """Decorator for registering keys."""
    def f(method: KeyMeth) -> KeyMeth:
        callbacks[name] = method
        return method
    return f


# Rotation stuff:
angle = np.pi / 24
Rxy = np.array([[np.cos(angle), np.sin(angle), 0],
                [-np.sin(angle), np.cos(angle), 0],
                [0, 0, 1]])
Rxz = Rxy[[0, 2, 1]][:, [0, 2, 1]]
Ryz = Rxy[[2, 1, 0]][:, [2, 1, 0]]


class AAMol:
    """Main AAMol object."""
    def __init__(self,
                 molecules: List[Molecule],
                 ui: UserInterface,
                 show_help_message: bool = True):
        self.molecules = molecules
        self.view = View()
        self.ui = ui
        self.canvas = ui.get_canvas()
        self.repeated = np.ones(3, int)
        self.output = StringIO()
        if show_help_message:
            self.message = _('Press "h" for help')
        else:
            self.message = ''
        self.show_info = True
        self.label = 'symbol'
        self.colorbar = 'off'
        self.config_number = 0
        self.seconds = 0.0
        self.frames = 0
        self.focus_cell()

    @property
    def molecule(self) -> Molecule:
        """Current molecule."""
        return self.molecules[self.config_number]

    def run(self) -> None:
        """Start program."""
        stdout = sys.stdout
        sys.stdout = self.output
        ex = None
        try:
            self.mainloop(stdout)
        except KeyboardInterrupt:
            print(f'FPS: {self.frames / self.seconds:.1f}')
        except Exception as x:
            self.ui.stop()
            ex = x
        finally:
            sys.stdout = stdout
        if ex:
            print(self.output.getvalue(), end='')
            raise ex

    def mainloop(self, stdout: TextIO) -> None:
        """Mainloop: Display, wait for keypress, do something, ..."""
        while True:
            t1 = time()
            plot(self.molecule, self.view, self.canvas,
                 self.label, self.colorbar)
            if self.message:
                self.canvas.paste(self.message, pad=1)

            self.canvas.paste(
                f'{self.config_number + 1}/{len(self.molecules)}', x=-1, y=-1)

            if self.show_info:
                self.canvas.paste(self.molecule.info, x=0, y=0)

            sys.stdout = stdout
            try:
                self.ui.show(self.canvas)
            finally:
                sys.stdout = self.output

            t2 = time()
            self.seconds += t2 - t1
            self.frames += 1

            self.message = ''
            key = self.ui.get_key()
            method = callbacks.get(key)
            if method is not None:
                method(self)
            else:
                assert not self.ui.testing

    @key('f')
    def focus(self) -> None:
        """Make all atoms visible."""
        self.view.focus(self.canvas, self.molecule)

    @key('F')
    def focus_cell(self) -> None:
        """Make all atoms and all of the unit cell visible."""
        self.view.focus(self.canvas, self.molecule, include_cell=True)

    @key('h')
    def help(self) -> None:
        """Show help."""
        self.message = _('HELP')

    @key('+')
    def zoom_in(self) -> None:
        """Zoom in."""
        self.view.scale *= 1.25

    @key('-')
    def zoom_out(self) -> None:
        """Zoom out."""
        self.view.scale /= 1.25

    @key('S-right')
    def shift_right(self) -> None:
        self.view.shift([-1, 0])

    @key('S-left')
    def shift_left(self) -> None:
        self.view.shift([1, 0])

    @key('S-up')
    def shift_up(self) -> None:
        self.view.shift([0, -1])

    @key('S-down')
    def shift_down(self) -> None:
        self.view.shift([0, 1])

    @key('up')
    def rotate_yz(self) -> None:
        self.view.rotate(Ryz.T)

    @key('down')
    def rotate_zy(self) -> None:
        self.view.rotate(Ryz)

    @key('right')
    def rotate_xz(self) -> None:
        self.view.rotate(Rxz.T)

    @key('left')
    def rotate_zx(self) -> None:
        self.view.rotate(Rxz)

    @key('A-right')
    def rotate_xy(self) -> None:
        self.view.rotate(Rxy.T)

    @key('A-left')
    def rotate_yx(self) -> None:
        self.view.rotate(Rxy)

    @key('resize')
    def resize(self) -> None:
        """Adapt to resized screen."""
        oldsize = self.canvas.size
        newsize = self.ui.get_size()
        self.view.scale *= (np.array(newsize) / oldsize).min()
        self.canvas = Canvas(newsize)

    @key('q')
    def quit(self) -> None:
        raise KeyboardInterrupt

    @key('1')
    def repeat1(self) -> None:
        """Repeat cell one more time along 1. axis."""
        self.repeat_cell(0, 1)

    @key('!')
    def repeat_1(self) -> None:
        """Repeat cell one less time along 1. axis."""
        self.repeat_cell(0, -1)

    @key('2')
    def repeat2(self) -> None:
        """Repeat cell one more time along 2. axis."""
        self.repeat_cell(1, 1)

    @key('"')
    def repeat_2(self) -> None:
        """Repeat cell one less time along 2. axis."""
        self.repeat_cell(1, -1)

    @key('3')
    def repeat3(self) -> None:
        """Repeat cell one more time along 3. axis."""
        self.repeat_cell(2, 1)

    @key('#')
    def repeat_3(self) -> None:
        """Repeat cell one less time along 3. axis."""
        self.repeat_cell(2, -1)

    def repeat_cell(self, axis: int, change: int) -> None:
        if change == 1 or self.repeated[axis] > 1:
            self.repeated[axis] += change
            self.molecules = [molecule.repeat(self.repeated)
                              for molecule in self.molecules]

    @key('previous-page')
    def previous(self) -> None:
        """Show previous molecule."""
        self.config_number = max(0, self.config_number - 1)

    @key('next-page')
    def next(self) -> None:
        """Show next molecule."""
        self.config_number = min(len(self.molecules) - 1,
                                 self.config_number + 1)

    @key('i')
    def toggle_info(self) -> None:
        self.show_info ^= True

    @key('l')
    def toggle_label(self) -> None:
        labels = ['symbol', 'absolute_coordinates',
                  'sequence_number', 'forces', 'magmoms', 'off']
        i = labels.index(self.label)
        self.label = labels[(i + 1) % len(labels)]

    @key('c')
    def toggle_colorbar(self) -> None:
        colorbars = ['forces', 'magmoms', 'off']
        i = colorbars.index(self.colorbar)
        self.colorbar = colorbars[(i + 1) % len(colorbars)]
