"""Command-line interface."""
import argparse
import locale
import re
import subprocess
import sys
from pathlib import Path
from typing import List

from .molecule import read_molecules, Molecule
from .l10n import _, set_language


aamol_molecule = Molecule(
    ['Aa', 'Mo', 'L'],
    [(1, 2, 2), (2, 2, 2), (3, 2, 2)],
    [(4, 0, 0), (0, 4, 0), (0, 0, 4)],
    extra={'magmoms': [0, 2, 1],
           'forces': [(1, 0, 0), (0, 0, 0), (-1, 0, 0)]})


def main(arguments: List[str] = None, testing: bool = False) -> int:
    """Entry point for command line tool."""
    from .api import open_molecules
    from .config import read_config_file, write_first_config_file
    from .text_mode import write_summary

    language = locale.getdefaultlocale()[0]
    if language:
        set_language(language)

    parser = argparse.ArgumentParser(
        description='Visualize atoms using ascii-art.',
        epilog=_('Manipulate molecule with the following keys: ') +
        _('HELP').replace('\n', '.  '))
    add = parser.add_argument

    add('file', nargs='*', help='Name of file to read from.  '
        'Use filename@SELECTION to only read part of the file.  '
        'See --selection for more information.')
    add('-V', '--show-version', action='store_true',
        help=_('Show version number and location of source code.'))
    add('-v', '--verbose', action='store_true',
        help=_('Be more verbose.'))
    add('-k', '--keys', default='',
        help='Keys to press after launch.  Example: -k 111 will repeat '
        'system three times along 1st unit cell axis.')
    add('-t', '--text', action='store_true',
        help=_('Text mode.'))
    add('-s', '--selection', default=':', help=_('SELECTION'))
    add('-w', '--new-window', action='store_true',
        help='Open in new window.  Can be configured in your '
        '~/.config/aamol/config.py file.')

    args = parser.parse_args(arguments)

    config = read_config_file()
    if not config:
        write_first_config_file()

    if args.show_version:
        from . import __version__
        print(_('Version:'), __version__)
        print(_('Code:'), Path(__file__).parent)
        return 0

    if args.new_window:
        # Remove new-window option to avoid infinite loop
        if arguments is None:
            arguments = sys.argv
        for option in ['-w', '--new-window']:
            if option in arguments:
                arguments.remove(option)
                break
        else:
            for i, option in enumerate(arguments):
                if option.startswith('-w'):
                    arguments[i] = '-' + option[2:]
                    break
            else:
                assert False, arguments  # -vw perhaps
        cmd = config['open_new_terminal']
        subprocess.check_call(f'{cmd} ' + ' '.join(arguments), shell=True)
        return 0

    if not sys.stdin.isatty() and not testing:
        args.file[:0] = ['-']

    if args.file:
        try:
            molecules = read_files(args.file, args.selection)
        except (FileNotFoundError, ImportError) as ex:
            print(ex, file=sys.stderr)
            return 1
    else:
        molecules = [aamol_molecule]

    if not sys.stdin.isatty() and not testing:
        # Reopen stdin so that curses works
        import os
        f = open('/dev/tty')
        os.dup2(f.fileno(), 0)

    show_help_message = config.get('show_help_message', True)
    assert isinstance(show_help_message, bool)

    if args.text:
        write_summary(molecules, args.verbose)
    else:
        open_molecules(molecules,
                       keys=list(args.keys),
                       show_help_message=show_help_message,
                       verbose=args.verbose)

    return 0


def read_files(filenames: List[str], selection: str) -> List[Molecule]:
    """Read molecules from files.

    Use selection if filename doesn't have its own @SELECTION extension."""
    molecules: List[Molecule] = []
    for filename in filenames:
        if re.search('@[-:0-9]+$', filename):
            filename, sel = filename.rsplit('@')
        else:
            sel = selection
        molecules += read_molecules(Path(filename), sel)
    return molecules
