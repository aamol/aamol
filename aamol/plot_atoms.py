"""Convert molecule to "pixels"."""
from .atomic_data import covalent_radii as radii, jmol_colors
from .molecule import Molecule
from .view import View
from .canvas import Canvas


def plot(molecule: Molecule,
         view: View,
         canvas: Canvas,
         label: str = 'symbol',
         colorbar: str = 'off') -> None:
    """Draw molecule on canvas as seen from view."""

    coords = view.transform(molecule.positions)
    coords[:, :2] += canvas.real_size / 2
    indices = coords[:, 2].argsort()

    canvas.clear()

    labels = molecule.get_labels(label)

    if colorbar == 'off':
        colors = molecule.symbols
        # Initialize colors if canvas doesn't already know them
        for color in set(colors):
            if color not in canvas.colors:
                rgb = jmol_colors.get(color, (0, 1, 1))
                canvas.add_color(color, rgb)
    else:
        colors, cmin, cmax = molecule.get_colors(colorbar)
        canvas.initialize_colorbar_colors()

    # Draw spheres with labels
    for i in indices:
        p = coords[i]
        symbol = molecule.symbols[i]
        r = radii.get(symbol, 1.7) * view.scale * 0.8
        canvas.circle(p, r, colors[i])
        p[2] += r + 1e-10
        canvas.write(p, labels[i], 'black')

    # Draw unit cell
    points = molecule.periodic + 1
    corner = view.transform([0, 0, 0])
    axes = view.transform(molecule.cell) - corner
    corner[:2] += canvas.real_size / 2

    for i0 in range(3):
        if points[i0] == 1:
            continue
        ax = axes[i0]
        for i1 in range(points[i0 - 1]):
            for i2 in range(points[i0 - 2]):
                start = corner + i1 * axes[i0 - 1] + i2 * axes[i0 - 2]
                canvas.line(start, start + ax, 'cell')

    if colorbar != 'off':
        for i in range(21):
            canvas.paste('  ', 'black', f'CM-{i}', 0, 27 - i)
        canvas.paste(f'{cmin:.2f}', x=0, y=28)
        canvas.paste(f'{cmax:.2f}', x=0, y=6)
