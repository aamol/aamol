"""Top level module: Defines a version number and a view() function."""
from typing import List, Union

import numpy as np

from .molecule import Molecule

__version__ = '20.3.0'


def view(configs: Union[Molecule, List[Molecule]],
         colors: List[float] = None) -> None:
    """View molecule(s).

    The molecules can be Molecule objects from AAMol or Atoms objects from
    ASE.  The colors must be given as one number for each atom.
    """
    from .api import open_molecules
    if not isinstance(configs, list):
        configs = [configs]
    molecules = [mol if isinstance(mol, Molecule) else Molecule.from_ase(mol)
                 for mol in configs]
    if colors is not None:
        for mol in molecules:
            mol.extra['magmoms'] = np.asarray(colors)
        keys = ['c', 'c']  # press 'c' for color twice to show magmom colors
    else:
        keys = []
    open_molecules(molecules, keys=keys)
