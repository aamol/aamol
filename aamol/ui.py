"""Base class for user-interface implementations.

For displaying text and getting keyboard input from user.
"""
import os
from typing import List, Tuple, Optional
from types import TracebackType

from .canvas import Canvas


class UserInterface:
    """Base class for user-interface implementations."""

    # Tests set this to avoid calling os.get_terminal_size()
    size: Optional[Tuple[int, int]] = None

    def __init__(self, keys: List[str] = None):
        self.keys = keys or []
        self.errors: List[str] = []
        self.testing = False

    def __enter__(self) -> 'UserInterface':
        return self

    def __exit__(self,
                 exc_type: Exception,
                 exc_value: Exception,
                 tb: TracebackType) -> None:
        self.stop()

    def stop(self) -> None:
        """Stop UI and print error messages."""
        for error in self.errors:
            print(error)

    def show(self, canvas: Canvas) -> None:
        """Show what is on the canvas."""
        _ = canvas.lines

    def get_key(self) -> str:
        """Wait for user to press a key and return it as a string."""
        if self.keys:
            return self.keys.pop(0)
        return 'q'

    def get_size(self) -> Tuple[int, int]:
        """"Return (width, height) tuple."""
        return self.size or os.get_terminal_size()

    def get_canvas(self) -> Canvas:
        """"Get a canvas to draw on."""
        size = self.get_size()
        return Canvas(size)
