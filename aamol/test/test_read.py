from pathlib import Path

from aamol.cli import read_files
from aamol.molecule import read_molecules


def test_reading(tmp_path: Path) -> None:
    p1 = tmp_path / '1.xyz'
    p1.write_text('1\n\nH 0 0 0\n')
    m = read_molecules(p1)[0]
    assert m.symbols == ['H']
    m = read_files([str(p1)], ':')[0]
    assert m.symbols == ['H']
