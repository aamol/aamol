from aamol.molecule import Molecule


def test_extra_from_ase():
    from ase import Atoms
    from ase.calculators.emt import EMT
    a = Atoms('CO', positions=[(0, 0, 0), (1, 0, 0)])
    a.calc = EMT()
    m = Molecule.from_ase(a)
    assert len(m.extra) == 0
    a.get_forces()
    m = Molecule.from_ase(a)
    assert 'forces' in m.extra
