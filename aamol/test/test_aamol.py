import pytest

from aamol.api import open_molecules
from aamol.cli import aamol_molecule
from aamol.molecule import Molecule
from aamol.ui import UserInterface

molecules = [aamol_molecule,
             Molecule([]),
             Molecule(['H'], [[0, 0, 0]], [[1, 0, 0], [0, 0, 0], [0, 0, 0]])]


@pytest.mark.parametrize('molecules',
                         [[mol] for mol in molecules] + [molecules])
def test_aamol(molecules):
    keys = (['up', 'left', 'down', 'right', 'A-right', 'A-left',
             'next-page',
             'S-up', 'S-left', 'S-down', 'S-right'] +
            list('h123123!"#fFllllllii+-ccc') +
            ['next-page'] +
            list('h123123!"#fFllllllii+-ccc') +
            ['next-page', 'previous-page', 'resize'])
    ui = UserInterface(keys=keys)
    ui.size = (70, 25)
    ui.testing = True
    open_molecules(molecules, ui=ui)
    ui.keys = ['?']
    with pytest.raises(AssertionError):
        open_molecules(molecules, ui=ui)
