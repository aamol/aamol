from aamol.molecule import Molecule
from aamol.view import View
from aamol.canvas import Canvas
from aamol.plot_atoms import plot


def test1():
    m = Molecule()
    v = View()
    assert len(v.transform(m.positions)) == 0


def test2():
    c = Canvas((6, 3))
    m = Molecule(['Au', 'Ba', 'Cu'],
                 [[1, 0, 0],
                  [1, 0, 1],
                  [2, 0, 0]])
    v = View()
    v.focus(c, m, 2)
    print(v)
    plot(m, v, c)
    print(c)
