from aamol.molecule import Molecule


def test_bulk():
    a = 2.3
    b = Molecule(['Li'],
                 [[0, 0, 0]],
                 [[a, 0, 0], [0, a, 0], [0, 0, a]])
    b2 = b.repeat([2, 1, 1])
    print(b2)
    b3 = b2.repeat([2, 2, 2])
    print(b3)
