import curses
from typing import Union

from ase import Atoms

from aamol import view
from aamol.cli import main
from aamol.curses_ui import CursesUserInterface


def test_version():
    assert main(['-V']) == 0


class Win:
    def keypad(self, x: bool) -> None:
        ...

    def clear(self) -> None:
        ...

    def move(self, y, x) -> None:
        self.x = x
        self.y = y
        assert 0 <= y < 25
        assert 0 <= x < 70

    def addstr(self, c, p) -> None:
        if self.x == 69 and self.y == 24:
            self.raised = True
            raise curses.error
        self.x += 1

    def refresh(self) -> None:
        ...

    def get_wch(self) -> Union[str, int]:
        return 'q'


p = 0


def color_pair(n: int) -> int:
    global p
    p += 1
    return p


def test_curses(monkeypatch, tmp_path):
    win = Win()
    monkeypatch.setattr(curses, 'initscr', lambda: win)
    monkeypatch.setattr(curses, 'noecho', lambda: None)
    monkeypatch.setattr(curses, 'cbreak', lambda: None)
    monkeypatch.setattr(curses, 'echo', lambda: None)
    monkeypatch.setattr(curses, 'nocbreak', lambda: None)
    monkeypatch.setattr(curses, 'endwin', lambda: None)
    monkeypatch.setattr(curses, 'start_color', lambda: None)
    monkeypatch.setattr(curses, 'init_color', lambda n, r, g, b: None)
    monkeypatch.setattr(curses, 'init_pair', lambda n, f, b: None)
    monkeypatch.setattr(curses, 'color_pair', color_pair)
    CursesUserInterface.size = (70, 25)
    xyz = tmp_path / 'h.xyz'
    xyz.write_text('1\n\nH 0 0 0\n')
    assert main(['-k', '+-', str(xyz)], testing=True) == 0
    assert main(['asdf'], testing=True) == 1
    assert win.raised
    view([Atoms()])
    view(Atoms())
    view(Atoms('H2'), colors=[-1.0, 1.0])
    key = curses.KEY_UP
    assert CursesUserInterface().key_name(key) == f'{key} KEY_UP'


def test_text_mode(capsys):
    assert main(['-tv'], testing=True) == 0
    captured = capsys.readouterr()
    out = captured.out
    checks = 0
    for line in out.splitlines():
        if line.startswith('Magmoms:'):
            mom = float(line.split()[-1][:-1])
            assert mom == 2.0
            checks += 1
        elif line.startswith('Fmax:'):
            fmax = float(line.split()[1])
            assert fmax == 1.0
            checks += 1
    assert checks == 2


def test_new_window(monkeypatch):
    import aamol.config
    monkeypatch.setattr(aamol.config, 'read_config_file',
                        lambda: {'open_new_terminal': 'echo'})
    assert main(['-w'], testing=True) == 0
    assert main(['-wv'], testing=True) == 0
