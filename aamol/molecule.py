"""Molecule class.

Can also represent bulk, slab and chain systems.
"""
import io
import sys
from collections import Counter
from math import acos, pi
from pathlib import Path
from typing import List, Sequence, Dict, Any, Union, Tuple

import numpy as np

Float1d = Sequence[float]
Float2d = Sequence[Float1d]


class Molecule:
    """Container to hold chemical symbols, positions.  Can also have a
    unit cell, forces, magnetic moments and colors.
    """
    def __init__(self,
                 symbols: List[str] = [],
                 positions: Float2d = [],
                 cell: Float2d = np.zeros((3, 3)),
                 extra: Dict[str, Union[Float1d, Float2d]] = {}):
        self.symbols = symbols[:]
        self.positions = np.array(positions, dtype=float)
        self.cell = np.array(cell, dtype=float)
        self.extra: Dict[str, np.ndarray] = {name: np.array(array)
                                             for name, array in extra.items()}
        if len(self.positions) == 0:
            self.positions.shape = (0, 3)
        self.periodic = self.cell.any(axis=1)
        self.repeated = np.ones(3, int)
        self._info = ''
        self._labels: Dict[str, List[str]] = {}
        self._colors: Dict[str, Tuple[List[str], float, float]] = {}

    @classmethod
    def from_ase(self, atoms: Any) -> 'Molecule':
        """Create molecule from ASE Atoms object."""
        extra = {}
        calc = atoms.calc
        if calc is not None:
            mms = calc.get_property('magmoms', allow_calculation=False)
            if mms is not None and mms.any():
                extra['magmoms'] = mms
            forces = calc.get_property('forces', allow_calculation=False)
            if forces is not None:
                extra['forces'] = forces
        return Molecule(list(atoms.symbols),
                        atoms.positions,
                        atoms.cell,
                        extra)

    @property
    def info(self) -> str:
        """Extract important information.

        * chemical formula
        * unit cell
        * spacegroup
        * maximum force
        * range of magnetic moments
        """
        if self._info:
            return self._info

        r = np.prod(self.repeated)
        count = Counter(self.symbols[:len(self) // r])
        formula = ' + '.join(f'{n} * {symbol}'
                             for symbol, n in count.items())
        if r > 1:
            formula = '{}*{}*{}: {}'.format(*self.repeated, formula)

        C = self.cell
        L = (C**2).sum(1)**0.5
        A = []
        for i in range(3):
            LL = L[i - 1] * L[i - 2]
            if LL:
                a = acos(C[i - 1].dot(C[i - 2]) / LL) / pi * 180
                A.append(f'{a:.3f}')
            else:
                A.append('-')

        n = len(self) // r
        s = 's' if n != 1 else ''
        self._info = (f'{formula} = {n} atom{s}\n'
                      f'[Å]: {L[0]:.3f}, {L[1]:.3f}, {L[2]:.3f}\n'
                      f'[°]: {A[0]}, {A[1]}, {A[2]}\n')

        try:
            import spglib
        except ImportError:
            ...
        else:
            numbers = []
            symb2num: Dict[str, int] = {}
            for symbol in self.symbols:
                n = symb2num.get(symbol)
                if n is None:
                    n = len(symb2num)
                    symb2num[symbol] = n
                numbers.append(n)
            sg = spglib.get_spacegroup((self.cell,
                                        self.positions,
                                        numbers))
            self._info += f'{sg}\n'

        mms = self.extra.get('magmoms')
        if mms is not None:
            self._info += f'Magmoms: [{mms.min():.2f}, {mms.max():.2f}]\n'

        forces = self.extra.get('forces')
        if forces is not None:
            f2 = (forces**2).sum(1)
            i = f2.argmax()
            self._info += f'Fmax: {f2[i]**0.5:.2f} eV/Å (#{i})\n'

        return self._info

    def __str__(self) -> str:
        return f'Molecule({self.symbols}, {self.positions}, {self.cell})'

    def __len__(self) -> int:
        return len(self.symbols)

    def repeat(self, repeat: Sequence[int]) -> 'Molecule':
        """Repeat system along unit cell axes."""
        repeated = np.array(repeat)
        repeated[~self.periodic] = 1
        n = repeated.prod()
        norig = self.repeated.prod()
        natomsorig = len(self) // norig
        positions = np.tile(self.positions[:natomsorig], (n, 1))
        n1 = 0
        for r0 in range(repeated[0]):
            for r1 in range(repeated[1]):
                for r2 in range(repeated[2]):
                    n2 = n1 + natomsorig
                    positions[n1:n2] += np.dot((r0, r1, r2), self.cell)
                    n1 = n2
        molecule = Molecule(self.symbols[:natomsorig] * n,
                            positions,
                            self.cell)
        molecule.repeated = repeated
        return molecule

    def get_labels(self, kind: str) -> List[str]:
        """Get labels for atoms.

        kind must be one of symbol, absolute_coordinates, forces,
        magmoms or sequence_number.
        """
        labels = self._labels.get(kind)
        if labels is None:
            if kind == 'symbol':
                return self.symbols
            if kind == 'absolute_coordinates':
                labels = [f'({x:.2f},{y:.2f},{z:.2f})'
                          for x, y, z in self.positions]
            elif kind == 'forces' and 'forces' in self.extra:
                labels = [f'[{x:.2f},{y:.2f},{z:.2f}]'
                          for x, y, z in self.extra['forces']]
            elif kind == 'magmoms' and 'magmoms' in self.extra:
                labels = [f'{m:.2f}'
                          for m in self.extra['magmoms']]
            elif kind == 'sequence_number':
                labels = [str(i) for i in range(len(self))]
            else:
                labels = [''] * len(self)
            self._labels[kind] = labels
        return labels

    def get_colors(self, kind: str) -> Tuple[List[str], float, float]:
        """Convert forces or magnetic moments to colors."""
        colors = self._colors.get(kind)
        if colors is None:
            if kind == 'forces' and 'forces' in self.extra:
                f = (self.extra['forces']**2).sum(1)**0.5
                colors = get_colors(f, 0, f.max())
            elif kind == 'magmoms' and 'magmoms' in self.extra:
                m = self.extra['magmoms']
                colors = get_colors(m, m.min(), m.max())
            else:
                colors = (self.symbols, 0.0, 0.0)
            self._colors[kind] = colors
        return colors


def get_colors(x: np.ndarray,
               xmin: float,
               xmax: float) -> Tuple[List[str], float, float]:
    """Convert numbers to colors."""
    if xmin == xmax:
        return (['CM-10'] * len(x), xmin, xmax)
    return ([f'CM-{s:.0f}'
             for s in ((x - xmin) / (xmax - xmin) * 20).round()],
            xmin, xmax)


def read_molecules(path: Path, selection: str = ':') -> List[Molecule]:
    """Read molecule(s) from file."""
    from ase.io import read
    if path == Path('-'):
        atoms_objects = read(io.StringIO(sys.stdin.read()),
                             selection,
                             format='xyz')
    else:
        atoms_objects = read(path, selection)
    if not isinstance(atoms_objects, list):
        atoms_objects = [atoms_objects]
    return [Molecule.from_ase(atoms) for atoms in atoms_objects]
