"""Write summary for text-mode."""

from typing import List

from .molecule import Molecule


def write_summary(molecules: List[Molecule], verbose: bool = False) -> None:
    """Write summary."""
    for molecule in molecules:
        print(molecule.info)
        if verbose:
            print(molecule)
