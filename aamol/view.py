"""View object defined by center, rotation and a scale factor."""
from typing import List

import numpy as np

from .canvas import Canvas
from .molecule import Molecule


class View:
    """View object defined by center, rotation and a scale factor."""
    def __init__(self) -> None:
        self.rotation = np.eye(3)
        self.center = np.zeros(3)
        self.scale = 1.0

    def __str__(self) -> str:
        R = self.rotation.tolist()
        return f'<View R={R} s={self.scale} offset={self.center}>'

    def transform(self, positions: np.ndarray) -> np.ndarray:
        """Transform to view-space."""
        return (positions - self.center).dot(self.scale * self.rotation)

    def focus(self,
              canvas: Canvas,
              molecule: Molecule,
              padding: float = 1.5,
              include_cell: bool = False) -> None:
        """Focus view on molecule."""
        coords = molecule.positions.dot(self.rotation)
        if include_cell and molecule.periodic.any():
            indices = np.indices(molecule.periodic + 1).reshape((3, -1)).T
            corners = indices.dot(molecule.cell).dot(self.rotation)
            coords = np.concatenate((coords, corners))
        if len(coords) == 0:
            return
        size = coords.ptp(axis=0)
        self.center = np.linalg.solve(self.rotation.T,
                                      coords.min(axis=0) + size / 2)
        self.scale = (canvas.real_size / (size[:2] + 2 * padding)).min()

    def shift(self, delta: np.ndarray) -> None:
        """Shift center of view."""
        c = np.zeros(3)
        c[:2] = np.asarray(delta) / self.scale
        self.center += np.linalg.solve(self.rotation.T, c)

    def rotate(self, R: List[List[float]]) -> None:
        """Set rotation."""
        self.rotation = self.rotation.dot(R)
