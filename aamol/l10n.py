"""Localization of strings.
"""
from typing import Dict

__all__ = ['_']

da = {
    'Code:':
        'Kode:',
    'Can\'t read {path}.  Try "pip install ase"':
        'Kan ikke læse {path}.  Prøv "pip install ase"',
    'Press "h" for help':
        'Tryk "h" for hjælp',
    'HELP':
        """\
h: hjælp
f: fokusér
F: fokusér (inklusiv enhedscellen)
l: skift etiket
c: farveskala (slukket -> kræfter -> magnetiske momenter)
i: tænd/sluk info-boks
+: zoom ind
-: zoom ud
q,CTRL+c: afslut
side-op,ned: næste/forrige billede
1,2,3: gentag mere langs 1., 2. eller 3. akse
!,",#: gentag mindre langs 1., 2. eller 3. akse
op,ned,højre,venstre,ALT+højre,venstre: rotér
SKIFT+op,ned,højre,venstre: translatér
F11: fuld skærm
"""}

en: Dict[str, str] = {
    'HELP':
        """\
h: help
f: focus
F: focus (include unit-cell)
l: switch label
c: colorbar (off -> forces -> magnetic moments)
i: toggle info-box
+: zoom in
-: zoom out
q,CTRL+c: quit
page-up,down: next/previous image
1,2,3: repeat more along 1st, 2nd or 3rd axis
!,",#: repeat less along 1st, 2nd or 3rd axis
up,down,right,left+ALT+right,left: rotate
SHIFT+up,down,right,left: translate
F11: full screen""",
    'SELECTION':
        """\
Pick individual image or slice from each of the files.
SLICE can be a number or a Python slice-like
expression such as :STOP, START:STOP, or
START:STOP:STEP, where START, STOP, and STEP are
integers. Indexing counts from 0. Negative numbers
count backwards from last image. Using @SLICE syntax
for a filename overrides this option for that file."""}

dictionary = en


def set_language(lang: str) -> None:
    """Set language."""
    global dictionary
    if lang.startswith('da'):
        dictionary = da


def _(word: str) -> str:
    """Translate string to current language."""
    return dictionary.get(word, en.get(word, word))
