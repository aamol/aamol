"""Configuration-file stuff."""
from pathlib import Path
from typing import Dict, Union, Any


def read_config_file() -> Dict[str, Union[bool, str]]:
    """Read configuration file (a Python file)."""
    cfg = Path.home() / '.config/aamol/config.py'
    if cfg.is_file():
        namespace: Dict[str, Any] = {}
        exec(compile(cfg.read_text(), str(cfg), 'exec'), namespace)
        return namespace.get('config', {})
    return {}


config = """\
import os
term = os.environ.get('COLORTERM', 'gnome-terminal')
config = {
    'show_help_message': False,
    'open_new_terminal': f'{term} --geometry 80x40 --quiet --'}
"""


def write_first_config_file(path: Path = None) -> None:
    """Write the first configuration file."""
    path = path or Path.home() / '.config/aamol/config.py'
    if not path.parent.is_dir():
        path.parent.mkdir(parents=True)
    path.write_text(config)
