"""Curses user-interface."""

import curses
import functools
from typing import Union, List, Dict, Tuple, Any

from .canvas import Canvas
from .ui import UserInterface

key_names = {
    curses.KEY_UP: 'up',
    curses.KEY_DOWN: 'down',
    curses.KEY_RIGHT: 'right',
    curses.KEY_LEFT: 'left',
    curses.KEY_SRIGHT: 'S-right',
    curses.KEY_SLEFT: 'S-left',
    curses.KEY_SF: 'S-up',
    curses.KEY_SR: 'S-down',
    curses.KEY_RESIZE: 'resize',
    curses.KEY_NPAGE: 'next-page',
    curses.KEY_PPAGE: 'previous-page',
    543: 'A-right',
    558: 'A-left'}


def get_wch(win: Any) -> Union[str, int]:
    """Fallback for curses without unicode."""
    key = win.getch()
    if key < 127:
        return chr(key)
    return key


class CursesUserInterface(UserInterface):
    """Simple curses interface."""
    def __init__(self, keys: List[str] = None):
        UserInterface.__init__(self, keys)
        self.colors: Dict[int, int] = {}
        self.color_pairs: Dict[Tuple[int, int], int] = {}

    def __enter__(self) -> 'CursesUserInterface':
        self.win = curses.initscr()
        self.get_wch = getattr(self.win, 'get_wch',
                               functools.partial(get_wch, self.win))
        curses.noecho()
        curses.cbreak()
        self.win.keypad(True)
        curses.start_color()
        return self

    def stop(self) -> None:
        self.win.keypad(False)
        curses.echo()
        curses.nocbreak()
        curses.endwin()
        UserInterface.stop(self)

    def color(self, canvas: Canvas, id: int) -> int:
        c = self.colors.get(id)
        if c is None:
            c = len(self.colors)
            curses.init_color(c, *(int(x * 1000) for x in canvas.rgb[id]))
            self.colors[id] = c
        return c

    def color_pair(self, canvas: Canvas, x: int, y: int) -> int:
        fg = canvas.fg[x, y]
        bg = canvas.bg[x, y]
        c = self.color_pairs.get((fg, bg))
        if c is None:
            n = len(self.color_pairs) + 1
            curses.init_pair(n, self.color(canvas, fg), self.color(canvas, bg))
            c = curses.color_pair(n)
            self.color_pairs[(fg, bg)] = c
        return c

    def show(self, canvas: Canvas) -> None:
        self.win.clear()
        for y, line in enumerate(canvas.lines):
            self.win.move(y, 0)
            for x, c in enumerate(line):
                try:
                    self.win.addstr(c, self.color_pair(canvas, x, y))
                except curses.error:
                    assert (x + 1, y + 1) == canvas.size
        self.win.refresh()

    def get_key(self) -> str:
        """Wait for keypress."""
        if self.keys:
            return self.keys.pop(0)
        ch = self.get_wch()
        if isinstance(ch, str):
            return ch
        return key_names.get(ch, '?')

    def key_name(self, key: Union[int, str]) -> str:
        name = f'{key}'
        for attr in dir(curses):
            if getattr(curses, attr) == key:
                name += f' {attr}'
                break
        return name
